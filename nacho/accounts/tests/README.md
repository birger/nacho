This folder contains the unit tests for `nacho`.
The tests use `Faker` for creating fake test data (mostly
usernames and passwords) and `volatildap` for creating
a slapd instance to connect to.

The test cases are divided into groups: first there is
the `CodeBreakerPyFlakes` test case, which uses `pyflakes`
to test if the code is PEP8 conform.
* ConnectionTestCase: tests the basic ldap connection
* NachoLdapTestCase: tests if the directory is set up correctly
* NachoWebTestCase: tests the basic functionality of the nacho
webinterface including user registration and email confirmation
* NachoProfileTestCase: tests the management of a user profile
and its attributes
* NachoAdminTestCase: test login as staff member and management
of user accounts via the admin interface
