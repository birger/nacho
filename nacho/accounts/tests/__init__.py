# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#

import os
import re
import subprocess
import textwrap
import volatildap
import ldap
import unittest.mock as mock
import datetime
# import ldap.modlist as modlist

from faker import Faker

from django.conf import settings
from django.core import management, mail
from django.test import TestCase
from django.utils.crypto import get_random_string
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from ..models import LdapUser, LdapGroup, LdapTemporaryUser
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(
                           os.path.dirname(os.path.abspath(__file__)))))

SSH_PUBLICKEY_RSA = 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAA'\
    'ABAQDkLl8ttPvOlXvvysHT/KoXG/QWjw9dzhrCw8XhnfpQ+4qz'\
    'BF3sHbuxt7Yd12aUCZTJZ4fZMDVwsD3HKKHVoAjxBf3HLR5tIF'\
    'aaZoGVK1RNUv2IF3xrVlxtW1I3ULGEUb2h32SJ9fFkY19GerAC'\
    's4na93hMdQM8lBoWbnhEMOlo8Esjh1LYCmTm1Uy5Zua1VavB53'\
    'jAQ7Wz1gQqsUYFPBnaHUMJShRgt41cSnyPac88FZ9eGBfoAXZI'\
    'C/UMkUXT4W6jx6Jq6yuhfEaS4InJhEQ4dOQiqiKHY5AHhj/jGF'\
    'CWlqJSA0KeEdrqd4WqaNBZ3yBzPbkhdtsQfQDf2/jrWZJr'
SSH_PUBLICKEY_25519 = 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE'\
    '5AAAAINBSvDi32W2T3FduiczrLABmLG3knki8xhzV76x2CvNZ'
SSH_PUBLICKEY_DSA = 'ssh-dss AAAAB3NzaC1kc3MAAACBAIGgBZhGk'\
    'P/lRUk84ROmdlG/IEbhXdGoDb8ySFisRVJo9xtkewaEEWZlNET'\
    'NX6amn8yQ/EHn4smSJMH0rdLY/YhY/LX46jj0/vcE8ddqGA9MI'\
    'X1bgUO4+FMacxQ+bW6uhOyS5qw42xyb+5ogFNm5R9tfjpyfCSL'\
    'wbdP6i73HjxJ5AAAAFQDlBSlkax8vmTiFFk13RwDZ+2mMtwAAA'\
    'IAAjHHe/KylowmmH8aUYzUaf9MSo6LmsaDltVMkQOZ/CwmGanK'\
    'IhhiS/XlNf29Itdi+BMmqtwTDmExFwt5pb3MBYfrZ0QLPDVoJB'\
    '0dB17vlvTIW3F4L9iSX2Lzxkf27IkXdJ5HU1FIbke7L4KTUWH1'\
    'rRfIlKNQAdS4iFNlMIqBYCwAAAIAuwhl/6yv4EhiDM+5EcVqYC'\
    'Mj1TVuYb5GjbB8C2xXP0Ofb7GA1nJdxSc2Jl64y+f52So4CnPS'\
    'N5Z8ChQLdV/1SKcvzxPtkU5xGppmzILPWsUv8sjfldw4gGgGum'\
    'PKcrFlj1DrhtWrqaqZQLbpOqSgQFkOrSiPLgyiSrh3LOjWC7g=='
SSH_PUBLICKEY_ECDSA = 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5'\
    'AAAAIJrPVqtxrFLb4xvodPKUPe99IM9UWDXcqIAUT61BFcRA '\
    'test@foobar'
TEST_PASSWORD = 'Passwort123'
TEST_PASSWORD_HASH = '{SSHA}1wZn/TBKLhmQivSjixQi4gdNzFaXrSmp'

# base
groups = ('ou=groups,dc=example,dc=org', {
    'objectClass': ['top', 'organizationalUnit'],
    'ou': ['groups']})
people = ('ou=people,dc=example,dc=org', {
    'objectClass': ['top', 'organizationalUnit'],
    'ou': ['people']})
temp = ('ou=temp,dc=example,dc=org', {
    'objectClass': ['top', 'organizationalUnit'],
    'ou': ['temp']})

# groups
foogroup = ('cn=foogroup,ou=groups,dc=example,dc=org', {
    'objectClass': ['posixGroup'],
    'cn': ['foogroup'],
    'memberUid': ['foouser-guest', 'baruser-guest'],
    'gidNumber': ['1000']})
staff = ('cn=staff,ou=groups,dc=example,dc=org', {
    'objectClass': ['posixGroup'],
    'cn': ['staff'],
    'memberUid': ['foouser-guest'],
    'gidNumber': ['1003']})

# ldapuser
foouserguest = ('uid=foouser-guest,ou=people,dc=example,dc=org', {
    'objectClass': ['shadowAccount', 'inetOrgPerson', 'ldapPublicKey'],
    'uid': ['foouser-guest'],
    'userPassword': [TEST_PASSWORD_HASH.encode('ascii')],
    'cn': [b'F\xc3\xb4o Us\xc3\xa9r'],
    'sn': [b'Us\xc3\xa9r'],
    'mail': [b'foouser@example.org'],
    'sshPublicKey': [SSH_PUBLICKEY_RSA.encode('ascii')]})
baruserguest = ('uid=baruser-guest,ou=people,dc=example,dc=org', {
    'objectClass': ['shadowAccount', 'inetOrgPerson', 'ldapPublicKey'],
    'uid': ['baruser-guest'],
    'userPassword': [TEST_PASSWORD_HASH.encode('ascii')],
    'cn': [b'Bar'],
    'sn': [b'User'],
    'mail': [b'baruser@example.net'],
    'sshPublicKey': [SSH_PUBLICKEY_RSA.encode('ascii')]})
wizuserguest = ('uid=wizuser-guest,ou=people,dc=example,dc=org', {
    'objectClass': ['shadowAccount', 'inetOrgPerson', 'ldapPublicKey'],
    'uid': ['wizuser-guest'],
    'userPassword': [TEST_PASSWORD_HASH.encode('ascii')],
    'cn': [b'Wiz'],
    'sn': [b'User'],
    'mail': [b'wizuser@example.org']})

# temporaryldapuser
bobguest = ('uid=bob-guest,ou=temp,dc=example,dc=org', {
    'objectClass': ['shadowAccount', 'inetOrgPerson', 'ldapPublicKey'],
    'uid': ['bob-guest'],
    'cn': [b'Bob'],
    'sn': [b'River']})

directory = dict([groups,
                  foogroup,
                  staff,
                  people,
                  foouserguest,
                  baruserguest,
                  wizuserguest,
                  temp,
                  bobguest])


def gettokenlink(tokentype, text):
    needle = '/' + tokentype + '/[0-9A-Za-z_\-]+/'\
        '[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20}/'
    return re.findall(needle, text)


class User:
    def __init__(self):
        self.fake = Faker()
        self.username = self.fake.user_name()
        self.cn = self.fake.last_name()
        self.sn = self.fake.first_name()
        self.mail = self.fake.email()
        self.password = self.fake.password(length=32)

    def guestname(self):
        return self.username + "-guest"


class CodeBreakerPyFlakes(TestCase):
    def test_accounts(self):
        self.maxDiff = None
        proc = subprocess.Popen('flake8 nacho',
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=True)
        procOut = proc.communicate()[0].decode('ascii')
        self.assertEqual(procOut, '')  # no output means success


class BaseTestCase(TestCase):
    directory = {}

    @classmethod
    def setUpClass(cls):
        super(BaseTestCase, cls).setUpClass()
        cls.ldap_server = volatildap.LdapServer(
            initial_data=cls.directory,
            schemas=[
                'core.schema',
                'cosine.schema',
                'inetorgperson.schema',
                'nis.schema',
                os.path.join(BASE_DIR, 'openssh-ldap.schema')],
        )
        settings.DATABASES['ldap']['USER'] = cls.ldap_server.rootdn
        settings.DATABASES['ldap']['PASSWORD'] = cls.ldap_server.rootpw
        settings.DATABASES['ldap']['NAME'] = cls.ldap_server.uri
        settings.AUTH_LDAP_SERVER_URI = cls.ldap_server.uri
        template = 'uid=%(user)s,ou=people,dc=example,dc=org'
        settings.AUTH_LDAP_USER_DN_TEMPLATE = template
        settings.AUTHENTICATION_BACKENDS = [
            'django_auth_ldap.backend.LDAPBackend',
            'django.contrib.auth.backends.ModelBackend']
        # settings.AUTH_LDAP_GROUP_TYPE = PosixGroupType(name_attr="cn")

    @classmethod
    def tearDownClass(cls):
        cls.ldap_server.stop()
        super(BaseTestCase, cls).tearDownClass()

    def setUp(self):
        super(BaseTestCase, self).setUp()
        self.ldap_server.start()


class ConnectionTestCase(BaseTestCase):
    directory = directory

    def test_system_checks(self):
        management.call_command('check')

    def test_make_migrations(self):
        management.call_command('makemigrations', dry_run=True)

    def test_ldap_auth_admin(self):
        ldapcon = ldap.initialize(self.ldap_server.uri)
        ret = False
        try:
            ldapcon.simple_bind_s(self.ldap_server.rootdn,
                                  self.ldap_server.rootpw)
            ret = True
        except ldap.LDAPError as e:
            ret = False
        self.assertTrue(ret)

    def test_ldap_auth_user(self):
        ldapcon = ldap.initialize(self.ldap_server.uri)
        ret = False
        try:
            ldapcon.simple_bind_s('uid=foouser-guest,ou=people'
                                  ',dc=example,dc=org', TEST_PASSWORD)
            ret = True
        except ldap.LDAPError as e:
            ret = False
        self.assertTrue(ret)

    # def test_ldap_passwd(self):
    # This is a test to get the passwd functionality working
    #    l = ldap.initialize(self.ldap_server.uri)
    #    l.simple_bind_s(self.ldap_server.rootdn, self.ldap_server.rootpw)
    #    attrs = {
    #            'cn': ['config'.encode()],
    #            'olcAccess': ['to attrs=userPassword by self write'
    # ' by anonymous auth by dn="cn=admin,dc=example,dc=org" write'
    # ' by * none'.encode()]
    #            }
    #    ldif = modlist.addModlist(attrs)
    #    l.add_s("cn=config", ldif)
    #    print(l.search_s('cn=config', ldap.SCOPE_SUBTREE))
    #    ret = False
    #    try:
    #        l.simple_bind_s("uid=foouser-guest,ou=people,dc=example,dc=org",
    #                        TEST_PASSWORD)
    #        l.passwd_s("uid=foouser-guest,ou=people,dc=example,dc=org",
    #                   TEST_PASSWORD, get_random_string(32))
    #        ret = True
    #    except ldap.LDAPError as e:
    #        ret = False
    #    self.assertTrue(ret)


class NachoLdapTestCase(BaseTestCase):
    directory = directory

    def test_count_groups(self):
        qs = LdapGroup.objects.all()
        self.assertEqual(qs.count(), 2)

    def test_count_people(self):
        qs = LdapUser.objects.all()
        self.assertEqual(qs.count(), 3)

    def test_count_temp(self):
        qs = LdapTemporaryUser.objects.all()
        self.assertEqual(qs.count(), 1)

    def test_login(self):
        self.assertTrue(self.client.login(username='foouser-guest',
                                          password=TEST_PASSWORD))

    def test_wrong_login(self):
        self.assertFalse(self.client.login(username=get_random_string(8),
                                           password=get_random_string(8)))


class NachoWebTestCase(BaseTestCase):
    "Show registration and related functionality"
    directory = directory

    def setUp(self):
        super(NachoWebTestCase, self).setUp()
        self._user = User()

    def test_root_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_404_page(self):
        response = self.client.get('/loginz/')
        self.assertEqual(response.status_code, 404)

    def test_login_page(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_redirect_with_auth(self):
        response = self.client.post('/login/',
                                    {'username': 'foouser-guest',
                                     'password': TEST_PASSWORD},
                                    follow=True)
        self.assertRedirects(response, '/accounts/profile/')

    def test_wrong_login(self):
        response = self.client.post('/login/',
                                    {'username': 'foouser',
                                     'password': TEST_PASSWORD},
                                    follow=True)
        expect = "Your username and password didn't match"
        self.assertContains(response, expect)

    def test_signup_page(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_successful_signup(self):
        response = self.client.post('/signup/', {
            'username': self._user.username,
            'mail': self._user.mail,
            'cn': self._user.cn,
            'sn': self._user.sn,
            'password1': self._user.password,
            'password2': self._user.password},
            follow=True)
        self.assertEqual(response.status_code, 200)

    def test_signup_token_message(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        expect = 'We have sent you a message with an activation link.'
        self.assertContains(response, expect, html=True)

    def test_temporaryldapuser_name(self):
        randuser = User()
        self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        u = LdapTemporaryUser.objects.get(username=randuser.guestname())
        self.assertEqual(u.mail, randuser.mail)

    def test_mailsent(self):
        randuser = User()
        self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(len(mail.outbox), 1)

    def test_mailsubject(self):
        randuser = User()
        self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(mail.outbox[0].subject, 'Activate Your Account')

    def test_mailcontainstoken(self):
        randuser = User()
        self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        m = gettokenlink('activate', mail.outbox[0].body)
        self.assertEqual(len(m), 1)

    def test_invalidtoken(self):
        randtoken = '/activate/YmlyZ2VyNg/4w4-9bda75d36787e47d748f/'
        response = self.client.get(randtoken)
        self.assertContains(response, "This token is invalid.")

    def test_accesstokenlink(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        m = gettokenlink('activate', mail.outbox[0].body)
        response = self.client.get(m[0], follow=True)
        self.assertRedirects(response, '/login/')

    def test_successmessage(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        m = gettokenlink('activate', mail.outbox[0].body)
        response = self.client.get(m[0], follow=True)
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "success")
        expect = 'Account %s activated' % (randuser.guestname())
        self.assertTrue(expect in message.message)

    def test_login(self):
        randuser = User()
        self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password})
        m = gettokenlink('activate', mail.outbox[0].body)
        self.client.get(m[0])
        self.assertTrue(self.client.login(username=randuser.guestname(),
                        password=randuser.password))

    def test_signup_wo_cn(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'cn', 'This field is required.')
        self.assertContains(response, 'This field is required.')

    def test_signup_wo_sn(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'sn', 'This field is required.')
        self.assertContains(response, 'This field is required.')

    def test_signup_w_nonmatching_pw(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': get_random_string(length=32)},
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response,
                             'form',
                             'password2',
                             "The two password fields didn't match.")
        self.assertContains(response,
                            "The two password fields didn&#39;t match.",
                            html=True)

    def test_signup_w_short_pw(self):
        randuser = User()
        randuser.password = get_random_string(length=8)
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(response.status_code, 200)
        expect = 'This password is too short. '\
            'It must contain at least 12 characters.'
        self.assertFormError(response,
                             'form',
                             'password2',
                             expect)
        self.assertContains(response,
                            expect,
                            html=True)

    def test_signup_w_pwismail(self):
        randuser = User()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.mail,
            'password2': randuser.mail},
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response,
                             'form',
                             'password2',
                             "The password is too similar to the mail.")
        self.assertContains(response,
                            "The password is too similar to the mail.",
                            html=True)

    def test_signup_w_existing_user(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        response = self.client.post('/signup/', {
            'username': randuser.username,
            'mail': randuser.mail,
            'cn': randuser.cn,
            'sn': randuser.sn,
            'password1': randuser.password,
            'password2': randuser.password},
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form',
                             'username', "Username is already in use.")
        self.assertContains(response, "Username is already in use.", html=True)

    def test_password_reset_page_status_code(self):
        response = self.client.get('/password_reset/')
        self.assertEquals(response.status_code, 200)

    def test_password_reset_page_content(self):
        response = self.client.get('/password_reset/')
        self.assertContains(response, "Username:")

    def test_password_reset_redirect(self):
        response = self.client.post('/password_reset/', {
            'username': get_random_string(length=32)},
            follow=True)
        self.assertRedirects(response, '/login/')

    def test_password_reset_randnonexistuser_msg(self):
        response = self.client.post('/password_reset/', {
            'username': get_random_string(length=32)},
            follow=True)
        message = list(response.context.get('messages'))[0]
        self.assertEquals(message.tags, "success")
        self.assertTrue("Password reset email sent." in message.message)

    def test_password_reset_randnonexistuser_mail(self):
        self.client.post('/password_reset/', {
            'username': get_random_string(length=32)},
            follow=True)
        self.assertEqual(len(mail.outbox), 0)

    def test_password_reset_existinguser_mail(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        self.assertEqual(len(mail.outbox), 1)

    def test_password_reset_existinguser_mail_subject(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        self.assertEqual(mail.outbox[0].subject, 'Reset Your Password')

    def test_password_reset_existinguser_mail_body1(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        m = gettokenlink('reset', mail.outbox[0].body)
        self.assertEquals(len(m), 1)

    def test_password_reset_existinguser_mail_body2(self):
        response = self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        m = gettokenlink('reset', mail.outbox[0].body)
        response = self.client.get(m[0])
        self.assertContains(response, "New password:", html=True)
        self.assertContains(response, "New password confirmation:", html=True)

    def test_password_reset_token_redirect(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        m = gettokenlink('reset', mail.outbox[0].body)
        password = get_random_string(length=32)
        response = self.client.post(m[0], {
            'new_password1': password,
            'new_password2': password},
            follow=True)
        self.assertRedirects(response, '/login/')

    @mock.patch('time.time')
    def test_password_reset_token_timeout(self, mock_now):
        mock_now.return_value = datetime.datetime(2020, 12, 27).timestamp()
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        mock_now.return_value = datetime.datetime(2020, 12, 31).timestamp()
        m = gettokenlink('reset', mail.outbox[0].body)
        response = self.client.get(m[0])
        self.assertContains(response, "This token is invalid.", html=True)

    def test_password_reset(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        m = gettokenlink('reset', mail.outbox[0].body)
        password = get_random_string(length=32)
        response = self.client.post(m[0], {
            'new_password1': password,
            'new_password2': password},
            follow=True)
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, "success")
        self.assertTrue("Password reset." in message.message)

    def test_password_reset_w_login(self):
        self.client.post('/password_reset/', {
            'username': 'foouser-guest'
        }, follow=True)
        m = gettokenlink('reset', mail.outbox[0].body)
        password = get_random_string(length=32)
        self.client.post(m[0], {
            'new_password1': password,
            'new_password2': password},
            follow=True)
        self.assertTrue(self.client.login(
            username='foouser-guest', password=password))


class NachoProfileTestCase(BaseTestCase):
    directory = directory

    def test_profile(self):
        response = self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.get('/accounts/profile/')
        expect = '<a class="nav-link active"'\
            'href=/accounts/profile/>foouser-guest</a>'
        self.assertContains(response, expect, html=True)

    def test_group(self):
        response = self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        expect = '<span type="button" class="btn-sm btn-dark">foogroup</span>'
        self.assertContains(response, expect, html=True)

    def test_sshkeylisting(self):
        response = self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.get('/accounts/sshkeys/')
        sshkey = '\n'.join(textwrap.wrap(SSH_PUBLICKEY_RSA, 80))
        self.assertContains(response, '<pre>' + sshkey + '</pre>')

    def test_cn_sn_form(self):
        response = self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.get('/accounts/profile/update/')
        self.assertContains(response, 'name="cn" value="Bar"')
        self.assertContains(response, 'name="sn" value="User"')

    def test_cn_sn_form_update(self):
        response = self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.post('/accounts/profile/update/',
                                    {'sn': 'Blafasl', 'cn': "Blafasl"},
                                    follow=True)
        self.assertContains(response, '<td>Common Name</td>\n<td>Blafasl</td>',
                            html=True)
        self.assertContains(response,
                            '<td>Surname</td>\n<td>Blafasl</td>', html=True)

    def test_photo_upload1(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD})
        jpegPhoto = LdapUser.objects.get(username='baruser-guest').jpegPhoto
        filename = os.path.join(BASE_DIR,
                                'nacho/accounts/tests/profilepic.jpg')
        with open(filename, 'rb') as fp:
            self.client.post('/accounts/profile/update/', {
                'sn': get_random_string(8),
                'cn': get_random_string(8),
                'jpegPhoto': fp}, follow=True)
        u = LdapUser.objects.get(username='baruser-guest')
        self.assertNotEqual(jpegPhoto, u.jpegPhoto)

    def test_photo_upload2(self):
        self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD})
        filename = os.path.join(BASE_DIR,
                                'nacho/accounts/tests/profilepic.jpg')
        with open(filename, 'rb') as fp:
            self.client.post('/accounts/profile/update/', {
                'sn': get_random_string(8),
                'cn': get_random_string(8),
                'jpegPhoto': fp}, follow=True)
        u = LdapUser.objects.get(username='foouser-guest')
        profilepicbytes = open(filename, "rb").read()
        self.assertEqual(u.jpegPhoto, profilepicbytes)

    def test_wrong_photo_upload1(self):
        self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD})
        filename = os.path.join(BASE_DIR,
                                'nacho/accounts/tests/profilepic.png')
        with open(filename, 'rb') as fp:
            self.client.post('/accounts/profile/update/', {
                'sn': get_random_string(8),
                'cn': get_random_string(8),
                'jpegPhoto': fp}, follow=True)
        u = LdapUser.objects.get(username='foouser-guest')
        self.assertEqual(u.jpegPhoto, '')

    def test_wrong_photo_upload2(self):
        response = self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD})
        filename = os.path.join(BASE_DIR,
                                'nacho/accounts/tests/profilepic.png')
        with open(filename, 'rb') as fp:
            response = self.client.post('/accounts/profile/update/', {
                'sn': get_random_string(8),
                'cn': get_random_string(8),
                'jpegPhoto': fp}, follow=True)
        mimetypes = ",".join(settings.NACHO_PROFILE_IMG_MIMETYPES)
        maxsize = str(settings.NACHO_PROFILE_IMG_MAX_SIZE)
        self.assertContains(response, "File has to be one of " +
                            mimetypes + " and must be at most " +
                            maxsize + " bytes")

    def test_email_change_message(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        self.assertContains(response, 'Email change token '
                            'sent to foobar@example.org.')

    def test_email_change_mail(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        self.assertEqual(len(mail.outbox), 1)

    def test_email_change_mail_subject(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        self.assertEqual(mail.outbox[0].subject, 'Change Your Emailaddress')

    def test_email_change_mail_token(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        m = gettokenlink('emailupdate', mail.outbox[0].body)
        self.assertEquals(len(m), 1)

    def test_email_update_works(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        m = gettokenlink('emailupdate', mail.outbox[0].body)
        self.client.get(m[0])
        # TODO: check if page contains the correct alert

    def test_email_update_done(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/email/update/", {
            'mail': 'foobar@example.org'},
            follow=True)
        m = gettokenlink('emailupdate', mail.outbox[0].body)
        self.client.get(m[0])
        u = LdapUser.objects.get(username='baruser-guest')
        self.assertEqual(u.mail, 'foobar@example.org')

    def test_sshkey_delete_confirm_key1(self):
        response = self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.get("/accounts/sshkeys/rem/0", follow=True)
        self.assertContains(response, SSH_PUBLICKEY_RSA)

    def test_sshkey_delete_confirm_key2(self):
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/rem/0/", {
            'sshPublicKey': SSH_PUBLICKEY_RSA},
            follow=True)
        user = LdapUser.objects.get(username='baruser-guest')
        self.assertFalse(SSH_PUBLICKEY_RSA in user.sshPublicKey)

    def test_sshkey_delete_confirm_key3(self):
        user = LdapUser.objects.get(username='foouser-guest')
        origlen = len(user.sshPublicKey)
        self.client.post('/login/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/rem/0", {
            'sshPublicKey': SSH_PUBLICKEY_25519},
            follow=True)
        self.assertEqual(origlen, len(user.sshPublicKey))

    def test_sshkey_upload_page(self):
        response = self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.get("/accounts/sshkeys/add/")
        self.assertEqual(response.status_code, 200)

    def test_sshkey_upload_25519_1(self):
        user = LdapUser.objects.get(username='baruser-guest')
        origlen = len(user.sshPublicKey)
        self.client.post('/login/', {
            'username': 'baruser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/add/", {
            'sshPublicKey': SSH_PUBLICKEY_25519}, follow=True)
        user = LdapUser.objects.get(username='baruser-guest')
        self.assertEqual(origlen + 1, len(user.sshPublicKey))

    def test_sshkey_upload_25519_2(self):
        user = LdapUser.objects.get(username='wizuser-guest')
        self.client.post('/login/', {
            'username': 'wizuser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/add/", {
            'sshPublicKey': SSH_PUBLICKEY_25519}, follow=True)
        user = LdapUser.objects.get(username='wizuser-guest')
        self.assertTrue(SSH_PUBLICKEY_25519 in user.sshPublicKey)

    def test_sshkey_upload_ecdsa(self):
        user = LdapUser.objects.get(username='wizuser-guest')
        self.client.post('/login/', {
            'username': 'wizuser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/add/", {
            'sshPublicKey': SSH_PUBLICKEY_ECDSA}, follow=True)
        user = LdapUser.objects.get(username='wizuser-guest')
        self.assertTrue(SSH_PUBLICKEY_ECDSA in user.sshPublicKey)

    def test_sshkey_dsa_upload(self):
        user = LdapUser.objects.get(username='wizuser-guest')
        self.client.post('/login/', {
            'username': 'wizuser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        self.client.post("/accounts/sshkeys/add/", {
            'sshPublicKey': SSH_PUBLICKEY_DSA}, follow=True)
        user = LdapUser.objects.get(username='wizuser-guest')
        self.assertFalse(SSH_PUBLICKEY_DSA in user.sshPublicKey)

    def test_sshkey_wrong_format(self):
        self.client.post('/login/', {
            'username': 'wizuser-guest',
            'password': TEST_PASSWORD},
            follow=True)
        response = self.client.post("/accounts/sshkeys/add/", {
            'sshPublicKey': get_random_string(512)}, follow=True)
        msg = "Invalid Key: Couldn&#39;t find beginning of the key data"
        self.assertContains(response, msg)

    def test_account_delete_response(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        response = self.client.get("/accounts/delete/")
        self.assertEqual(response.status_code, 200)

    def test_account_delete_mail(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        self.client.post("/accounts/delete/", {
            'username': randuser.guestname()})
        self.assertEqual(len(mail.outbox), 1)

    def test_account_delete_mail_subject(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        self.client.post("/accounts/delete/", {
            'username': randuser.guestname()})
        self.assertEqual(mail.outbox[0].subject, 'Delete Your Account')

    def test_account_delete_mail_content(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        self.client.post("/accounts/delete/", {
            'username': randuser.guestname()})
        m = gettokenlink('delete', mail.outbox[0].body)
        self.assertEqual(len(m), 1)

    def test_account_delete_token(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        self.client.post("/accounts/delete/", {
            'username': randuser.guestname()})
        m = gettokenlink('delete', mail.outbox[0].body)
        response = self.client.get(m[0], follow=True)
        self.assertEqual(response.status_code, 200)

    def test_account_delete_wrong_token(self):
        path = '/delete/YmlyZ2VyNg/4w4-9bda75d36787e47d748f/'
        response = self.client.get(path, follow=True)
        self.assertContains(response, 'This token is invalid.')

    def test_account_delete(self):
        randuser = User()
        ldapuser = LdapUser(username=randuser.guestname(),
                            mail=randuser.mail,
                            cn=randuser.cn,
                            sn=randuser.sn)
        ldapuser.save()
        ldapuser.set_password(randuser.password)
        self.client.post('/login/', {
            'username': randuser.guestname(),
            'password': randuser.password},
            follow=True)
        self.client.post("/accounts/delete/", {
            'username': randuser.guestname()})
        m = gettokenlink('delete', mail.outbox[0].body)
        self.client.get(m[0], follow=True)
        self.assertFalse(self.client.login(username=randuser.guestname(),
                                           password=randuser.password))

    # def test_password_change(self):
    # TODO: volatildap doesn't provide passwd functionality
    #    password = get_random_string(32)
    #    response = self.client.post('/login/', {
    #        'username': 'wizuser-guest',
    #        'password': TEST_PASSWORD},
    #        follow=True)
    #    user = LdapUser.objects.get(username='wizuser-guest')
    #    print(user.userPassword)
    #    response = self.client.post("/password_change/", {
    #        'old_password': TEST_PASSWORD,
    #        'new_password1': password,
    #        'new_password2': password }, follow=True)
    #    user = LdapUser.objects.get(username='wizuser-guest')
    #    print(user.userPassword)
    #    self.assertNotEqual(user.userPassword, TEST_PASSWORD_HASH)


class NachoAdminTestCase(BaseTestCase):
    directory = directory

    def test_admin(self):
        response = self.client.get('/admin/login/')
        self.assertEquals(response.status_code, 200)

    def test_admin_staff_login(self):
        response = self.client.post('/admin/login/?next=/admin/', {
            'username': 'foouser-guest',
            'password': TEST_PASSWORD}, follow=True)
        self.assertRedirects(response, '/admin/')

    def test_admin_staff_content1(self):
        self.client.login(username='foouser-guest',
                          password=TEST_PASSWORD)
        response = self.client.get('/admin/')
        expect = "Recent actions"
        self.assertContains(response, expect)

    def test_admin_staff_content2(self):
        staff, created = Group.objects.get_or_create(name='staff')
        cts = ContentType.objects.filter(app_label='accounts')
        perms = Permission.objects.filter(content_type__in=cts)
        staff.permissions.add(*perms)
        self.client.login(username='foouser-guest',
                          password=TEST_PASSWORD)
        response = self.client.get('/admin/')
        expect = "Ldap user"
        self.assertContains(response, expect)
