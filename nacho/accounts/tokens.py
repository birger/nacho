# Copyright 2018 Nacho Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Nacho. It is subject to the license terms in the LICENSE
# file found in the top-level directory of this distribution. No part of Nacho,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#

import datetime
import time

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.crypto import constant_time_compare, salted_hmac
from django.utils.http import base36_to_int, int_to_base36


class NachoPasswordResetTokenGenerator(PasswordResetTokenGenerator):
    def make_token(self, user):
        return self._make_token_with_timestamp(
            user,
            self._num_hours(self._now()))

    def check_token(self, user, token):
        if not (user and token):
            return False
        try:
            ts_b36, hash = token.split("-")
        except ValueError:
            return False

        try:
            ts = base36_to_int(ts_b36)
        except ValueError:
            return False

        if not constant_time_compare(
                self._make_token_with_timestamp(user, ts),
                token):
            return False

        if (self._num_hours(self._now()) - ts) > \
                settings.NACHO_PASSWORD_RESET_TIMEOUT_HOURS:
            return False

        return True

    def _make_token_with_timestamp(self, user, timestamp):
        ts_b36 = int_to_base36(timestamp)
        hash = salted_hmac(
            self.key_salt,
            self._make_hash_value(user, timestamp),
            secret=self.secret,
        ).hexdigest()[::2]  # Limit to 20 characters to shorten the URL.
        return "%s-%s" % (ts_b36, hash)

    def _make_hash_value(self, user, timestamp):
        return (str(user.username) +
                str(user.mail) +
                str(user.userPassword) +
                str(timestamp))

    def _num_hours(self, now):
        then = datetime.datetime(year=2001, month=1, day=1, hour=0).timestamp()
        td = now - then
        return int(td / 60 / 60)

    def _now(self):
        # return datetime.datetime.now()
        return int(time.time())


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (str(user.username) +
                str(user.mail) +
                str(timestamp))


class EmailUpdateTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (str(user.username) +
                str(user.mail) +
                str(user.userPassword) +
                str(timestamp))


class UserDeleteTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (str(user.username) +
                str(user.mail) +
                str(user.userPassword) +
                str(timestamp))


account_activation_token = AccountActivationTokenGenerator()
nacho_password_reset_token = NachoPasswordResetTokenGenerator()
email_update_token = EmailUpdateTokenGenerator()
