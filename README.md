nacho is a django based web application for managing ldap accounts and groups.
it provides a simple interface for account-registration, self service (editing
profile information) and password reset as well as an interface for
administration of accounts.

![Screenshot of the profile page](Screenshot.jpg)

# Requirements

Debian packages: 
* python3-django (>2)
* python3-django-auth-ldap
* python3-django-ldapdb
* python3-magic
* python3-sshpubkeys

If you want to run tests
* python3-volatildap
* python3-fake-factory
* slapd

## Installation (package)

You can install the packaged version of nacho.
There is a default apache2 configuration in /etc/sites-available

## Installation (source)
```
mkdir ~/.virtualenvs
python3 -m venv ~/.virtualenvs/gsoc
source ~/.virtualenvs/gsoc/bin/activate
pip install django
pip install django-auth-ldap
pip install django-ldapdb
pip install python-magic
pip install sshpubkeys
```

and for running the tests:
```
pip install faker
pip install flake8
pip install volatildap
```

You can use the shipped nacho apache configuration from `debian/apache/nacho` and adapt
it so the `WSGIScriptAlias` points to the shipped wsgi.py file and also adapt the directory
paths (the admin path should point to djangos admin static files, the static path should
point to nachos static files)

## Configuration
copy the default settings from /usr/lib/python3/dist-packages/nacho/defaults.json to
/etc/nacho/config.json and modify to match your setup
the most important settings are the ldap server and user accounts and templates and the
path to the sqlite database. See [CONFIG.md](CONFIG.md)

If you install from source or if you run the debian package and change the
sqlitedbpath, you have to run those two commands to create the sqlite database:
```
python3 manage.py makemigrations
python3 manage.py migrate
```

## superuser & staff group

django has the concept of a superuser account- this account can do anything in `/admin`;
you have to create one of those using `python3 manage.py createsuperuser`;

django has one default group called 'staff' thats allowed to login into the admin interface on
`/admin`; you can define which group should have this permission using
`AUTH_LDAP_USER_FLAGS_BY_GROUP{'is_staff': ... }`; you also have to create this group in django
itself; use the superuser account for thath;

## Management commands

There are a couple of management commands

* cleanstaletempacc <n>: this command removes all temporary accounts that are older than <n> days

* backup: this command dumps the content of the ldap database in LDIF format to stdout

* restore: this command restores the ldap database from LDIF data from stdin

# Attribution

## Creative Commons Attribution 4.0 International by fontawesome.com https://creativecommons.org/licenses/by/4.0/

* static/img/fa/
  * edit.svg
  * envelope.svg
  * key.svg  
  * plus.svg
  * sign-out-alt.svg
  * user-circle.svg

## Bootstrap

* static/css/bootstrap.min.css
