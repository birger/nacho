# Nacho configuration

nacho uses a json formated configuration file; the defaults are stored in
defaults.json in the nacho directory.

These are the possible settings:

Your LDAP server
* "server": "ldap://127.0.0.1/",

The base DN of your LDAP installation
* "basedn": "dc=example,dc=org",

A LDAP account that has global read/write permissions
* "manager": "cn=Manager,dc=example,dc=org",

The password of the manager account
* "managerpw": "secret",

A template that defines where user objects are stored. Those
users are allowed to authenticate
* "userdntemplate": "uid=%(user)s,ou=people,dc=example,dc=org",

The Container where your LDAP groups are stored
* "groupcontainer": "ou=groups,dc=example,dc=org",

The attribute that defines your group name
* "posixgroupnameattr": "cn",

The path to the sqlite database nacho uses for session data, temporary authentication data...
* "sqlitedbpath": "/var/lib/nacho/db/db.sqlite3",

A list of strings that are not allowed to be used as username
* "nachoforbiddenuids": ["Manager", "admin"],

A string that is appended to each username
* "nachousernameappend": "-guest",

The maximum allowed size of profile images
* "nachoprofileimgmaxsize": 2000,

A list of formats allowed for profile images
* "nachoprofileimgmimetypes": ["image/jpeg"],

A list of allowed ssh public key types
* "nachosshkeytypes": ["ecdsa-sha2-nistp256", "ecdsa-sha2-nistp384", "ecdsa-sha2-nistp521", "ssh-ed25519", "ssh-rsa"],

The staff group is allowed to login in the admin interface at /admin
* "staffgroup": "staff",

This should be one of Djangos email backends, see https://docs.djangoproject.com/en/2.0/topics/email/#email-backends for details
Chance is that you want to use django.core.backends.smtp.EmailBackend
* "emailbackend": "django.core.mail.backends.console.EmailBackend",

Those are the settings for your email backend
* "emailhost": "localhost",
* "emailport": 25,
* "emailhostuser": "nobody",
* "emailhostpassword": "",
* "emailusetls": true,

The default From: address of mails
* "defaultfromemail": "nacho@example.org",

Debugging is disabled by default
* "debug": false,

The settings starting with `dbcol` specify the attributes where user fields are stored in:
* "dbcolsn": "sn",
* "dbcolcn": "cn",
* "dbcolmail": "mail",
* "dbcoljpegphoto": "jpegPhoto",
* "dbcolusername": "uid"
