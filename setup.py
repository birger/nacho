#!/usr/bin/env python
from setuptools import setup, find_packages
import os

data_files = []
for root, dirs, files in os.walk('nacho/static'):
    root_files = [os.path.join(root, i) for i in files]
    data_files.append(("/var/lib/"+root, root_files))
for root, dirs, files in os.walk('nacho/templates'):
    root_files = [os.path.join(root, i) for i in files]
    data_files.append(("/var/lib/"+root, root_files))

setup(
        name='nacho',
        version='0.1',
        packages=[
            '.'.join(directory.split(os.sep))
            for directory, _, files in os.walk('nacho')
            if '__init__.py' in files and
            files not in ['development']
        ],
        data_files=data_files,
	license='GPL v3',
	description='A simple Django app to (self)-manage ldap accounts.',
	url='https://salsa.debian.org/bisco-guest/nacho',
	author='Birger Schacht',
	author_email='birger@rantanplan.org',
        install_requires = [
            'django >= 2.0',
            'django-auth-ldap',
            'django-ldapdb',
            'python-magic',
            'sshpubkeys',
            'Faker',
            'flake8',
            'volatildap',
        ],
	classifiers=[
		'Environment :: Web Environment',
		'Framework :: Django',
		'Framework :: Django :: 2.0',
		'Programming Language :: Python',
		'Programming Language :: Python :: 3.5',
		'Programming Language :: Python :: 3.6',
	],
        test_suite="runtests.runtests"
)

