* validation for the username field should be moved
  from form validation to field validation
* when activating users, should the ou field get updated?
* tests, tests, tests...
* make utils.py python-ldap2 compatible (search result is empty not None)
* forward exceptions from utils.py: it doesn't warn the user if exists()
  or newuser() fail (i.e. in case of wrong credentials)
* user gdm user symbol instead of awesome font
* https://www.coderedcorp.com/blog/django-settings-for-multiple-environments/
